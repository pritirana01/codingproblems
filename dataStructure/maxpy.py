A = [1, 1, 2, 2, 2,3,3,3,3]

if len(A) == 1:
    majority_element = A[0]
else:
    length = len(A)
    count = 0  # Initialize count to 0
    max_count = 0  # Initialize max_count to 0
    max_element = None  # Initialize max_element to None

    for i in range(length):
        if count == 0:  # If count is 0, update max_element
            max_element = A[i]
            #print(max_element)
            count = 1
        elif A[i] == max_element: 
             # If current element is equal to max_element, increment count
            count += 1
        else:
            count -= 1  # If current element is not equal to max_element, decrement count

    # Count occurrences of max_element
    count = 0
    for num in A:
        
        if num == max_element:
            count += 1

    if count > 3// 2:
        majority_element = max_element
    else:
        majority_element = -1

print("Majority Element:", majority_element)

    

      
  


 