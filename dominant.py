#z=[2, 2, 2, 2, 2]
#z=[1, 2, 3, 4]
#z=[3 ,3, 2, 1]
z = [1, 1, 2, 2, 3, 4]
counts = {}
le = len(z)

for i in range(le):
    if z[i] in counts:
      counts[z[i]] += 1
    else:
        counts[z[i]] = 1

max_count = max(counts.values())

# Check if there is no dominant element
if list(counts.values()).count(max_count) > 1:
    print("NO dominant element")
else:
    print("YES")



