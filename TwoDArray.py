row = 2
col = 2
two = [[0 for j in range(col)] for i in range(row)]
two[0][0] = 1
two[0][1] = 0
two[1][0]=1
two[1][1]=0

for i in range(row):
    for j in range(i+1,col):
        two[i][j],two[j][i]=two[j][i],two[i][j]   # swap diagonally


for row in two:
     
    print(row)
