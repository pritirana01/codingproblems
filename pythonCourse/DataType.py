#integer-whole number without decimal
a=10
print(type(a))
#float-whole number with decimal
b=10.233
print(type(b))
#string-represent a sequence of  inside the double quattion
c="hello here is me"
print(type(c))
#list-represents an ordered collection, it is mutable means can be changed
d=[1,2,3,4]
print(d[1])
d[0]=10
print(d)
d1=["apple","mango","banana"]
print(d)
print(d1)
#Tuple-similar to list an order collection but we can no modify it
e=(1,3,4)
print(e,type(e))
print(e[1])
#e[2]=10
#print(e)  not mutable
#Dictionary-represent data in key-value pair..key are unique and we can not change and value can be anny data type
student={
    "name":"pooja",
    "age":20,
    "skills":["hockey","badminton"]

}
print(student,type(student))
#set ---an unordered collection of unique item ..not allow any duplicate item inside
f={1,2,3,4,5,5,4}
print(f,type(f))
#Boolean
is_rain=True
print(is_rain,type(is_rain))
#None-void value
g=None
print(g,type(g))  