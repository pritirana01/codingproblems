print("preeti","pooja")
print("shivam","neha")
#In Python, sep='' is a built-in feature within the print() function that allows you to specify the separator between multiple values you want to print. By default, the print() function separates values with a space and ends with a newline character (\n). However, by using sep='', you can change the separator to an empty string, meaning no separator will be added between the values. This effectively concatenates the values together without any additional characters. If you don't specify a sep parameter, Python will use its default separator behavior.
print("shivam","diva", sep="*")
print("pooja","nupur",sep="$")
print("diya","shivam", sep="\n")
print("nupur","diya","smaer","khusi", sep="*" ,end='&')
print("nupur","diya","smaer","khusi")