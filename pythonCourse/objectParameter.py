#Objects parameter refer to the type of data that we pass to a function and it can be any type like int,string,list,dictionary even custominzed
def Objects(obj):
    print("type of object",type(obj))
    print(obj)
Objects(2)   
Objects([1,2,3,4])
Objects({"name":"preect","Rollno":10})
Objects({"a":2 ,"b":3,"c":4})


def ObjectFun2(object1,Object2):
    print(object1,Object2)
ObjectFun2(1,2)  
ObjectFun2([1,2,3,4],[5,6,7]) 
ObjectFun2({"a":1,"b":2,"c":4,"d":5},{"e":6,"f":7,"g":8})

def add(a,b):
    c=a+b
    print(c)
add(1,3)
#sep-sep is abbrivation of seprator normally used with print function 
#we can not directly apply seprator with an array
print("how","are","you",sep="*")
el="hello how r u"
newel=el.split()
for i in range(len(newel)):
    print(newel[i],sep="*")


#end