#selection sort
listData = [11, 23, 1, 70, 4, 3]

for i in range(5):
    minpos = i
    for j in range(i, 6):
        if listData[j] < listData[minpos]:
            minpos = j
    # Swap the found minimum element with the first element
    temp = listData[i]
    listData[i] = listData[minpos]
    listData[minpos] = temp

# Print the sorted list
print(listData)
   